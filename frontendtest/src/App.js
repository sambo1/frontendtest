import React, { Component } from 'react'
import './App.css'
import Flicks from './components/Flicks'
import Navbar from './components/Navbar'
import { Overlay } from 'react-overlay';

class App extends Component {
  constructor (props) {
    super(props)
    this.state = {
      flicks: [],
      allFlicks: [],
      search: 'dogs'
    }
    this.updateSearch = this.updateSearch.bind(this)
    this.handleChange = this.handleChange.bind(this)
  }

  componentDidMount () {
    this.showImages()
  }

  showImages () {
    const url = `https://api.flickr.com/services/rest/?method=flickr.photos.search&tags=${this.state.search}&tagmode=all&api_key=6d22cf82ca92fb35f1d94e0048433157&format=json&nojsoncallback=1`
    fetch(url)
    .then(response => response.json())
    .then((data) => {
      this.setState({
        allFlicks: data.photos.photo,
        flicks: data.photos.photo
      })
    })
  }

  handleChange (e) {
    this.setState({search: e.target.value})
  }

  updateSearch (event) {
    this.setState({search: event.target.value.substr(0, 20)})
  }

  Delay = (function() {
    let timer = 0
    return function(callback, ms) {
      clearTimeout(timer)
      timer = setTimeout(callback, ms)
    }
  })()

  render () {

    return (
      <div className='App'>
        <div className='main'>
          <Navbar />
          <form>
            <div className='search'>
              <input type='text' placeholder='Search...'
                onChange={this.handleChange}
                onKeyUp={() => this.Delay(function(){
                  this.showImages()
                }.bind(this), 1000
                )} />
            </div>
          </form>

          <div className='flick'>
            {this.state.flicks.map((flick) => {
              return <Flicks key={flick.name} flick={flick} />
            })}
          </div>
        </div>
      </div>
    )
  }
}

export default App
