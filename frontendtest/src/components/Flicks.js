import React, { Component } from 'react'
import './Flicks.css'

class Flicks extends Component {
  render () {
    
    const title = this.props.flick.title
    const style = { backgroundImage: `url("https://farm${this.props.flick.farm}.staticflickr.com/${this.props.flick.server}/${this.props.flick.id}_${this.props.flick.secret}.jpg")`}
    
    return (
      <div className='flicks'>
        <div className='flicks-picture' style={style} />
        <div className='flicks-title'>{title}</div>    
      </div>
    )
  }
}

export default Flicks
